#include<stdio.h>

int main()
{
	int h, r;
	float vol;

	printf("Enter the height of the cone: ");
	scanf("%d", &h);

	printf("Enter the radius of the base of the cone: ");
        scanf("%d", &r);

	vol = (22/7.0) * r * r * h /3.0;

	printf("Volume of the cone is %.2f\n", vol);

	return 0;
}	
