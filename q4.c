#include<stdio.h>

int main()
{
	float c, f;

	printf("Enter temperature in degree Celsius : ");
	scanf("%f", &c);

	f = (c * 9.0/5.0) + 32;

	printf("The temperature in degrees Farenheit is %.2f\n", f);

	return 0;	
}
