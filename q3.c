#include<stdio.h>

int main()
{
	int x, y;

	printf("Enter the first number : ");
	scanf("%d", &x);

	printf("Enter the second number : ");
        scanf("%d", &y);

	printf("Before swapping the first number is %d and the second number is %d \n", x, y);

	x = x + y;
	y = x - y;
	x = x - y;

	printf("After swapping the first number is %d and the second number is %d\n", x, y);

	return 0;
}
