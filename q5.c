#include<stdio.h>

int main()
{
	int a = 10, b = 15;
		
	printf("A = %d , B = %d \n", a, b);

	printf("A & B = %d \n", a&b);
	printf("A ^ B = %d \n", a^b);
	printf("~A = %d \n", ~a);
	printf("A << 3 = %d \n", a<<3);
	printf("B >> 3 = %d \n", b>>3);

	return 0;
}
